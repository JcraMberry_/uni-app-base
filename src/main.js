import Vue from 'vue'
import App from './App'
import upperFirst from "lodash/upperFirst";
import camelCase from "lodash/camelCase";

Vue.config.productionTip = true

App.mpType = 'app'
// import cuCustom from './components/cu-costom/cu-custom.vue'
// Vue.component('cu-custom',cuCustom)


//全局注册常用组件  uni-app 不支持
// const requireComponent = require.context(
//     // 其组件目录的相对路径
//     './components/my-custom',
//     // 是否查询其子目录
//     false,
//     // 匹配基础组件文件名的正则表达式
//     // /Base[A-Z]\w+\.(vue|js)$/
//     /.*\.(vue|js)$/
// )
// requireComponent.keys().forEach(fileName => {
//     // 获取组件配置
//     const componentConfig = requireComponent(fileName)
//     const componentName = fileName
//         .split('/')
//         .pop()
//         .replace(/\.\w+$/, '')
//     console.info(componentName)
//     console.info(componentConfig)
//     // 全局注册组件
//     Vue.component(
//         componentName,
//         // 如果这个组件选项是通过 `export default` 导出的，
//         // 那么就会优先使用 `.default`，
//         // 否则回退到使用模块的根。
//         componentConfig.default || componentConfig
//     )
// })


const app = new Vue({
    ...App
})
app.$mount()
